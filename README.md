# Docker Tips

###### Dylan Hamel - Juin 2018 - <dylan.hamel@protonmail.com>



## 1. Les commandes 

* <u>**Rechercher**</u> des images dans le Docker Hub

```bash
docker search ubuntu
docker search microsoft
docker search centos
docker search apache
docker search mysql
docker search iptables
# Docker search avec le nom du service que l'on a besoin.
```



* <u>**Lister**</u> les container qui tournent

```bash
docker ps
➜  ~ docker ps
CONTAINER ID	IMAGE	COMMAND				CREATED			STATUS			PORTS	NAMES
896cde6d2fce	httpd	"httpd-foreground"	3 minutes ago	Up 3 minutes	80/tcp	admiri 
```

```bash
docker ps -a
# [-a] permet de lister tous les containers même dans un état arrêté.
➜  docker-tips git:(master) ✗ docker ps -a
CONTAINER ID        IMAGE                  COMMAND             CREATED             STATUS                       PORTS               NAMES
1c67ecd6838f        dylanhamel/gitlab-ce   "/assets/wrapper"   8 days ago          Exited (137) 6 seconds ago                       gitlab
```

```bash
# [-q] uniquement l'ID du container
➜  docker-tips git:(master) ✗ docker ps -aq
1c67ecd6838f
```



* <u>**Relancer**</u> un container 

```bash
docker start -i ba05ee0dedc6
# [OR]
docker start -i confident_hamilton

# [-i] = interactif
```



* <u>**Lancer**</u> un container 

```bash
docker run dylan-hamel/container
```

```bash
# Container HTTPD
# 1. Trouver un container avec le "search"
➜  ~ docker search httpd 
NAME	DESCRIPTION						STARS	OFFICIAL	AUTOMATED
httpd	The Apache HTTP Server Project	1796	[OK]                

# Lancer le container
➜  ~ docker run -d -ti vimagick/iptables
Unable to find image 'vimagick/iptables:latest' locally 
# si pas trouvé local search sur le Docker HUB
latest: Pulling from vimagick/iptables
# Effectue un PULL
ff3a5c916c92: Pull complete 
2d2a0e8c1170: Pull complete 
Digest: sha256:998160b9e6cddbd266aea6f77e81c7e3fb519d66e69a3fd51c0d08404419b005
Status: Downloaded newer image for vimagick/iptables:latest
ba05ee0dedc66d6dfbc8ef6b3fbc16cf84bd5d8edc60596ac943e6b7d4335ad2
```



* <u>**Éteindre**</u> un containter

```bash
docker kill admiring_jang
# [OR]
docker kill ba05ee0dedc6
```



* <u>**Supprimer**</u> un container

```bash
docker rm admiring_jang
# [OR]
docker rm ba05ee0dedc6
# [OR if UP]
docker rm -f ba05ee0dedc6
docker rm -f admiring_jang
```

* Plusieurs à la fois

```bash
docker kill $(docker ps -aq)
```

```bash
docker rm $(docker ps -aq)
```



* <u>**Téléchager**</u> la dernière mise à jour de l'image Docker.

  Avantage : uniquement la différence sera téléchargée --> Léger.

```bash
➜  ~ docker pull httpd
Using default tag: latest
latest: Pulling from library/httpd
Digest: sha256:963ecd717afb125c7a867d82d6935930bb93acc5078ea8bc37854d4b4de766d9
Status: Image is up to date for httpd:latest
```



* <u>**Lister**</u> les images téléchargées

```bash
docker images
```

```bash
➜  ~ docker images
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
httpd                 latest              2a7d646dbba8        5 days ago          178MB
vimagick/iptables     latest              528541c7c1a2        3 weeks ago         8.43MB
```



* <u>**Historique**</u> de création d'images

```bash
docker history httpd
```

```bash
➜  ~ docker history httpd
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
2a7d646dbba8        5 days ago          /bin/sh -c #(nop)  CMD ["httpd-foreground"]     0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  EXPOSE 80/tcp                0B                  
<missing>           5 days ago          /bin/sh -c #(nop) COPY file:761e313354b918b6…   133B                
<missing>           5 days ago          /bin/sh -c set -eux;   buildDeps="   bzip2  …   9.93MB              
<missing>           5 days ago          /bin/sh -c #(nop)  ENV APACHE_DIST_URLS=http…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV HTTPD_PATCHES=           0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV HTTPD_SHA256=de025118…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV HTTPD_VERSION=2.4.33     0B                  
<missing>           5 days ago          /bin/sh -c apt-get update  && apt-get instal…   40.8MB              
<missing>           5 days ago          /bin/sh -c {   echo 'deb http://deb.debian.o…   161B                
<missing>           5 days ago          /bin/sh -c #(nop)  ENV OPENSSL_VERSION=1.0.2…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV NGHTTP2_VERSION=1.18.…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop) WORKDIR /usr/local/apache2    0B                  
<missing>           5 days ago          /bin/sh -c mkdir -p "$HTTPD_PREFIX"  && chow…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV PATH=/usr/local/apach…   0B                  
<missing>           5 days ago          /bin/sh -c #(nop)  ENV HTTPD_PREFIX=/usr/loc…   0B                  
<missing>           5 days ago          /bin/sh -c echo 'deb http://deb.debian.org/d…   55B                 
<missing>           5 days ago          /bin/sh -c #(nop)  CMD ["bash"]                 0B                  
<missing>           5 days ago          /bin/sh -c #(nop) ADD file:d4f4a33443015d350…   127MB               
➜  ~ 
```



* <u>**Créer**</u> une nouvelle image après avoir modifier le container

```bash
docker commit CONTAINER_ID ProjetName
```



* Lien entre les container 

```bash
--link containerName
```





## 2. Run a Container 

* Faire tourner le container en arrière plan

```bash
# [-d]
docker run -d dylan-hamel/httpd
```

* <u>**Nommer**</u> le container

```bash
# [--name]
docker run -d --name apache dylan-hamel/httpd
```



* <u>**Port forwarding**</u> depuis la VM jusqu'au Container

```bash
# [-p] redirection sur le même port
docker run -d --name apache -p 80:80 -p 443:443 dylan-hamel/httpd
```

```bash
# [-p] redirection du port 8080 sur le 80
docker run -d --name apache -p 8080:80 dylan-hamel/httpd
```

```bash
# [-p] redirection d'un range de port
docker run -d --name ftp -p 21:21 -p 30000-30009:30000-30009  dfi/ftp
```

* <u>**Mapper**</u> un volume

```bash
# [-v] permet d'avoir de la persistence
docker run -d --name apache -v /data/httpd.conf /path/httpd.conf -p 8080:80 -p 8443:443 dylan-hamel/httpd
```

* <u>**Redémarrer**</u> le container avec Docker

```bash
# [--restart always]
docker run -d --restart always --name apache -v /data/httpd.conf /path/httpd.conf -p 8080:80 -p 8443:443 dylan-hamel/httpd 
```

* Setter une variable d'environnement

```bash
# [-e]
docker run -d --restart -e "HTTP_USER=admin" always --name apache -p 8080:80 -p 8443:443  -v /data/httpd.conf /path/httpd.conf  dylan-hamel/httpd 
```

* Setter le hostname du Container

```bash
# [--hostname]
docker run -d --restart -e "HTTP_USER=admin" always --name apache -p 8080:80 -p 8443:443  -v /data/httpd.conf /path/httpd.conf  --hostname srv-web01 dylan-hamel/httpd 
```



## 3. Lancer des commandes dans un Container

* Obtenir un Shell

```bash
docker exec -it containerNameOrID /bin/bash
```

* Redémarrer un service nginx / php

```bash
docker exec -u root sti_project service nginx start
docker exec -u root sti_project service php5-fpm start
```



## 4. Dockerfile

Permet de créer une nouvelle image avec des nouveaux paquets.
Exemple, besoin de ```vim``` et ```tcpdump``` sur un container ```httpd```

```dockerfile
FROM httpd:2.4
MAINTAINER Dylan Hamel <dylan.hamel@protonmail.com>

RUN apt-get update -y
RUN apt-get install -y vim tcpdump
RUN apt-get clean && rm -rf /trash/mydir

COPY ./public-html/ /usr/local/apache2/htdocs/
VOLUME ./logs /var/logs/messages

EXPOSE 80
EXPOSE 8080
EXPOSE 443

ENV HTTP_USER admin

ENTRYPOINT ["/usr/local/apache2/htdocs/"]
CMD [ "/scripts/run.sh" ]
```

Créer le container. Depuis le dossier ou se trouve le Dockerfile, lancer la commande

```bash
docker build -t dylan-hamel/httpd .
```



Cela correspond passablement au commande que l'on passe dans le ```docker run```

* ```FROM``` définit quelle image sur ```docker-hub``` utilisée

* ```MAINTAINER``` correspond à qui a créer ce ```Dockerfile```

* ```RUN``` permet d'exécuter une commande

* ```COPY``` va simplement copier un dossier de la machine hôte dans le container.

  * Si le dossier sur le container est modifié il ne sera pas modifié sur la machine hôte.
  * Il n'y a donc pas de persistance de données
  * Ne jamais utiliser ```COPY /data/httpd/log:/var/log/messages```

* ```VOLUME``` permet de mapper un volume commun entre le container et la machine hôte

* ```EXPOSE``` permet d'ouvrir le port ```80``` et de recevoir des données

* ```ENV``` permet de définir une variable d'environnement

* ```ENTRYPOINT``` définit le point d'entré du container

  * après avoir exécuter ```docker exec -it apache /bin/bash``` on arrivera dans ce dossier 

* ```CMD``` permet d'exécuter une commande 

  * Par exemple starter un service






## 5. Créer une Docker-Machine

* Sur VMWare

```bash
docker-machine create --driver vmwarefusion VM-NAME
```

* Sur VirtualBox

```bash
docker-machine create --driver=virtualbox VM-NAME
```

```bash
➜  MySQL docker-machine create --driver=virtualbox Docker   
Running pre-create checks...
Creating machine...
(Docker) Copying /Users/dylan.hamel/.docker/machine/cache/boot2docker.iso to /Users/dylan.hamel/.docker/machine/machines/Docker/boot2docker.iso...
(Docker) Creating VirtualBox VM...
(Docker) Creating SSH key...
(Docker) Starting the VM...
(Docker) Check network to re-create if needed...
(Docker) Waiting for an IP...
Waiting for machine to be running, this may take a few minutes...
Detecting operating system of created instance...
Waiting for SSH to be available...
Detecting the provisioner...
Provisioning with boot2docker...
Copying certs to the local machine directory...
Copying certs to the remote machine...
Setting Docker configuration on the remote daemon...
Checking connection to Docker...
Docker is up and running!
To see how to connect your Docker Client to the Docker Engine running on this virtual machine, run: docker-machine env Docker
```

Se connecter sur la Docker-Machine

```bash
docker-machine ssh vbox-test
```

```bash
➜  MySQL docker-machine ssh Docker                          
                        ##         .
                  ## ## ##        ==
               ## ## ## ## ##    ===
           /"""""""""""""""""\___/ ===
      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
           \______ o           __/
             \    \         __/
              \____\_______/
 _                 _   ____     _            _
| |__   ___   ___ | |_|___ \ __| | ___   ___| | _____ _ __
| '_ \ / _ \ / _ \| __| __) / _` |/ _ \ / __| |/ / _ \ '__|
| |_) | (_) | (_) | |_ / __/ (_| | (_) | (__|   <  __/ |
|_.__/ \___/ \___/ \__|_____\__,_|\___/ \___|_|\_\___|_|
Boot2Docker version 18.05.0-ce, build HEAD : b5d6989 - Thu May 10 16:35:28 UTC 2018
Docker version 18.05.0-ce, build f150324
docker@Docker:~$ 
```



* Version de la VM Docker

```bash
docker@Docker:~/MySQL$ uname -a
Linux Docker 4.9.93-boot2docker #1 SMP Thu May 10 16:27:54 UTC 2018 x86_64 GNU/Linux
```





## 6. Lancer un Container MySQL

* Télécharger les paquets MySQL

```bash
docker search mysql
docker pull mysql
# [OR]
docker pull mariadb
```



* Créer un container avec MySQL

  Il manque les variables d'environnement pour définir le mot de passe root ```MYSQL_ROOT_PASSWORD```

```bash
➜  ~ docker run mysql
error: database is uninitialized and password option is not specified 
  You need to specify one of MYSQL_ROOT_PASSWORD, MYSQL_ALLOW_EMPTY_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD
➜  ~ 
```



